package gsb.utils;

public class SQLDateChange {

    // Changement de la date afin de la stocker sur MySQL
    public static String SQLDateChange(String dateVisite) {
        String date = dateVisite;
        String[] decoupe = date.split("/");
        String dateSQL = decoupe[2]+"-"+decoupe[1]+"-"+decoupe[0];
        return dateSQL;
    }

}

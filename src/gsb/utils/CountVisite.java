package gsb.utils;

import gsb.modele.dao.VisiteDao;

public class CountVisite {

    // Compter de visite
    public static String CountVisite() {
        String reference;

        int referenceCount = VisiteDao.retournerCollectionDesVisites().size() + 1;
        if (referenceCount > 9 ) {
            reference = "v00" + referenceCount;
        } else if (referenceCount > 99) {
            reference = "v0" + referenceCount;
        } else if (referenceCount > 999) {
            reference = "v" + referenceCount;
        } else {
            reference = "v000" + referenceCount;
        }

        return reference;
    }

}

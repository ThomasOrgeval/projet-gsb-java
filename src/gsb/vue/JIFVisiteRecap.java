package gsb.vue;

import gsb.modele.Offrir;
import gsb.modele.Visite;
import gsb.modele.dao.OffrirDao;
import gsb.modele.dao.VisiteDao;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

class JIFVisiteRecap extends JInternalFrame {

    private static final long serialVersionUID = 1L;

    private JTextField JTreference, JTdateVisite, JTcommentaire, JTmatricule, JTcodeMedecin;
    private DefaultTableModel data;

    JIFVisiteRecap(String reference) {

        JPanel p = new JPanel();
        JPanel pTexte = new JPanel(new GridLayout(6, 2));

        JLabel JLreference = new JLabel("R�f�rence");
        JLabel JLdateVisite = new JLabel("Date visite");
        JLabel JLcommentaire = new JLabel("Commentaire");
        JLabel JLmatricule = new JLabel("Matricule visiteur");
        JLabel JLcodeMedecin = new JLabel("Code m�decin");

        JTreference = new JTextField(20);
        JTdateVisite = new JTextField();
        JTcommentaire = new JTextField();
        JTmatricule = new JTextField();
        JTcodeMedecin = new JTextField();

        pTexte.add(JLreference);
        pTexte.add(JTreference);
        pTexte.add(JLdateVisite);
        pTexte.add(JTdateVisite);
        pTexte.add(JLcommentaire);
        pTexte.add(JTcommentaire);
        pTexte.add(JLmatricule);
        pTexte.add(JTmatricule);
        pTexte.add(JLcodeMedecin);
        pTexte.add(JTcodeMedecin);

        String[] columnNames = {"", "Nom commercial", "Quantit� offerte"};
        data = new DefaultTableModel(columnNames, 0);
        JTable pTableau = new JTable(data);
        pTableau.getSelectionModel().addListSelectionListener(pTableau);

        JScrollPane scrollPane = new JScrollPane(pTableau);
        scrollPane.setPreferredSize(new Dimension(500, 100));

        Visite laVisite = VisiteDao.rechercher(reference);
        remplirText(laVisite);

        p.add(pTexte);
        p.add(scrollPane);
        Container contentPane = getContentPane();
        contentPane.add(p);

        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setTitle("R�capitulatif d'une visite");

    }

    private void remplirText(Visite laVisite) {
        JTreference.setText(laVisite.getReference());
        JTdateVisite.setText(laVisite.getDateVisite());
        JTcommentaire.setText(laVisite.getCommentaire());
        JTmatricule.setText(laVisite.getLeVisiteur().getMatricule());
        JTcodeMedecin.setText(laVisite.getLeMedecin().getCodeMed());

        JTreference.setEditable(false);
        JTdateVisite.setEditable(false);
        JTcommentaire.setEditable(false);
        JTmatricule.setEditable(false);
        JTcodeMedecin.setEditable(false);

        HashMap<String, Offrir> dicoMedicsOfferts = OffrirDao.retournerDictionnaireDesMedicsOfferts(laVisite.getReference());
        String[] donnee = new String[3];
        int i = 1;
        for (Map.Entry<String, Offrir> uneEntree : dicoMedicsOfferts.entrySet()) {
            donnee[0] = "M�dicament " + i;
            donnee[1] = uneEntree.getValue().getLeMedicament().getNomMedi();
            donnee[2] = String.valueOf(uneEntree.getValue().getQteOfferte());
            data.addRow(donnee);
            i++;
        }

    }

}

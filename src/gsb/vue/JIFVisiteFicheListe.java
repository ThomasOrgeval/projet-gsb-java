package gsb.vue;

import gsb.modele.Visite;
import gsb.modele.dao.VisiteDao;
import gsb.modele.dao.VisiteurDao;
import gsb.utils.SQLDateChange;
import gsb.utils.ValidationUtils;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

public class JIFVisiteFicheListe extends JIFVisiteFiche implements ActionListener {

    private static final long serialVersionUID = 1L;

    private JButton JBvalider, JBrecap;
    private DefaultTableModel data;
    private JTextField JTreference;

    JIFVisiteFicheListe() {
        // R�cup�ration de JIFVisiteFiche
        super();
        // Cr�ation du panneau pour le bouton r�capitulatif
        JPanel pRecap = new JPanel();

        // Initialisation du bouton Afficher pour compl�ter le tableau tTableau
        JBvalider = new JButton("Afficher");
        pBoutons.add(JBvalider);
        JBvalider.addActionListener(this);

        // Initialisation des ent�tes du tableau des visites
        String[] columnNames = {"R�f�rence", "Ville", "Commentaire", "Nom m�decin", "Code m�decin"};
        data = new DefaultTableModel(columnNames, 0);
        tTableau = new JTable(data);
        tTableau.getSelectionModel().addListSelectionListener(tTableau);

        scrollPane = new JScrollPane(tTableau);
        scrollPane.setPreferredSize(new Dimension(500, 100));
        p.add(scrollPane);

        // Ajout de la zone de texte pour une r�f�rence de visite
        JLabel JLreference = new JLabel("R�f�rence");
        JTreference = new JTextField(20);
        JTreference.addActionListener(this);
        pRecap.add(JLreference);
        pRecap.add(JTreference);

        // Ajout du bouton r�capitulatif pour lancer la page d'une visite
        JBrecap = new JButton("R�capitulatif");
        pRecap.add(JBrecap);
        JBrecap.addActionListener(this);

        p.add(pRecap);
        Container contentPane = getContentPane();
        contentPane.add(p);

        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setTitle("Liste des visites");
    }

    @Override
    public void actionPerformed(ActionEvent evt) {

        Object source = evt.getSource();
        // Action pour le remplissage du tableau des visites
        if (source == JBvalider || source == JTcodeVisiteur || source == JTdateVisite) {
            try {

                if (JTcodeVisiteur.getText().equals("") || JTdateVisite.getText().equals("")) {
                    throw new Exception("Donn�es obligatoires : Code visiteur, Date de visite");
                } else if (!ValidationUtils.isDateValide(JTdateVisite.getText())) {
                    throw new Exception("Le format de la date n'est pas valide");
                } else if (JTcodeVisiteur.getText().length() > 4) {
                    throw new Exception("Le matricule du visiteur ne fait pas 4 caract�res au maximum");
                } else if (VisiteurDao.rechercher(JTcodeVisiteur.getText()) == null) {
                    throw new Exception("Ce visiteur n'existe pas");
                } else {
                    String date = SQLDateChange.SQLDateChange(JTdateVisite.getText());
                    HashMap<String, Visite> dicoVisite = VisiteDao.retournerDictionnaireDesVisites(JTcodeVisiteur.getText(), date);
                    String[] donnee = new String[5];
                    // Lignes � afficher dans le tableau selon la date de visite et le code visiteur
                    for (Map.Entry<String, Visite> uneEntree : dicoVisite.entrySet()) {
                        donnee[0] = uneEntree.getValue().getReference();
                        donnee[1] = uneEntree.getValue().getLeMedecin().getLaLocalite().getVille();
                        donnee[2] = uneEntree.getValue().getCommentaire();
                        donnee[3] = uneEntree.getValue().getLeMedecin().getNom();
                        donnee[4] = uneEntree.getValue().getLeMedecin().getCodeMed();
                        data.addRow(donnee);
                    }
                }

            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, e);
            }

        }

        // Action pour l'ouverture de le r�capitulatif d'une visite
        if (source == JBrecap || source == JTreference) {
            try {

                if (JTreference.getText().equals("")) {
                    throw new Exception("Donn�es obligatoires : R�f�rence");
                } else if (JTreference.getText().length() != 5) {
                    throw new Exception("La r�f�rence n'est pas de 5 caract�res");
                } else if (VisiteDao.rechercher(JTreference.getText()) == null) {
                    throw new Exception("Cette visite n'existe pas");
                } else {
                    // Ouverture du r�capitulatif de la visite
                    MenuPrincipal.ouvrirFenetre(new JIFVisiteRecap(JTreference.getText()));
                }

            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, e);
            }
        }

    }

}

package gsb.vue;

import gsb.modele.Medicament;
import gsb.modele.dao.MedicamentDao;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.*;

public class JIFMedicamentCons extends JInternalFrame implements ActionListener {
    private static final long serialVersionUID = 1L;
    private ArrayList<Medicament> lesMedicament;

    protected JPanel p, pPrincipal, pHaut, pBas;
    protected JTextField JTcodeMedic;
    protected JScrollPane scrollPane;
    protected JButton JBafficher;
    protected JLabel JLcode;
    protected MenuPrincipal fenetreContainer;

    public JIFMedicamentCons(MenuPrincipal uneFenetreContainer) {

        fenetreContainer = uneFenetreContainer;
        JTable table;

        p = new JPanel();  // panneau principal de la fen�tre
        pPrincipal = new JPanel(new GridLayout(2, 1));
        pHaut = new JPanel((new GridLayout(1, 1)));
        pBas = new JPanel();


        lesMedicament = MedicamentDao.retournerCollectionDesMedicament();
        int nbLignes = lesMedicament.size();

        JLcode = new JLabel("Code");

        String[] titles = {"Code", "Nom", "Famille"};
        int i = 0;
        String[][] data = new String[nbLignes][4];
        for (Medicament unMedicament : lesMedicament) {
            data[i][0] = unMedicament.getCodeMedi();
            data[i][1] = unMedicament.getNomMedi();
            data[i][2] = unMedicament.getFamilleMedi();
            i++;
        }

        table = new JTable(data, titles);

        scrollPane = new JScrollPane(table);
        scrollPane.setPreferredSize(new Dimension(450, 200));

        JTcodeMedic = new JTextField(10);
        JTcodeMedic.setMaximumSize(JTcodeMedic.getPreferredSize());
        JTcodeMedic.addActionListener(this);

        JBafficher = new JButton("Fiche m�dicament d�taill�e");
        JBafficher.addActionListener(this);
        //afficher.addActionListener(this);

        Container contentPane = getContentPane();
        contentPane.add(p);

        p.add(pPrincipal);
        pPrincipal.add(pHaut);
        pPrincipal.add(pBas);

        pHaut.add(scrollPane);
        pBas.add(JLcode);
        pBas.add(JTcodeMedic);
        pBas.add(JBafficher);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {

        Object source = evt.getSource();

        if (source == JBafficher || source == JTcodeMedic) {
            try {

                if (JTcodeMedic.getText().equals("")) {
                    throw new Exception("Donn�es obligatoires : Code du m�dicament");
                }  else if (MedicamentDao.rechercher(JTcodeMedic.getText()) == null) {
                    throw new Exception("Ce m�dicament n'existe pas");
                } else {
                    MenuPrincipal.ouvrirFenetre(new JIFMedicamentRecap(JTcodeMedic.getText()));
                }

            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, e);
            }
        }


    }

}

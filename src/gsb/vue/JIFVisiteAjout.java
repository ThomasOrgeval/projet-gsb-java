package gsb.vue;

import gsb.modele.Medicament;
import gsb.modele.Stocker;
import gsb.modele.dao.*;
import gsb.service.OffrirService;
import gsb.service.StockerService;
import gsb.service.VisiteService;
import gsb.utils.ValidationUtils;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class JIFVisiteAjout extends JIFVisite implements ActionListener {

    private static final long serialVersionUID = 1L;
    private ArrayList<Medicament> lesMedics = MedicamentDao.retournerCollectionDesMedicament();
    private ArrayList<Stocker> lesStocks = StockerDao.retournerCollectionDesStocks();
    private String medicament1, medicament2, id1, id2;
    private int quantiteStock1, quantiteStock2;

    private JButton valider;
    private JComboBox comboMedics1, comboMedics2;
    private JTextField JTquantite1, JTquantite2;
    private JLabel JLmedic, JLquantite;

    public JIFVisiteAjout() {
        // R�cup�ration de JIFVisite
        super();

        String[] arrayLesMedics = new String[lesMedics.size()];
        int i = 0;
        for (Medicament unMedic : lesMedics) {
            arrayLesMedics[i] = unMedic.getNomMedi();
            i++;
        }
        comboMedics1 = new JComboBox(arrayLesMedics);
        comboMedics2 = new JComboBox(arrayLesMedics);

        JLmedic = new JLabel("D�p�t l�gal");
        JLquantite = new JLabel("Quantit�");

        JTquantite1 = new JTextField();
        JTquantite2 = new JTextField();

        JTquantite1.addActionListener(this);
        JTquantite2.addActionListener(this);

        pComboMedic.add(JLmedic);
        pComboMedic.add(comboMedics1);
        pComboMedic.add(comboMedics2);
        pComboMedic.add(JLquantite);
        pComboMedic.add(JTquantite1);
        pComboMedic.add(JTquantite2);

        // Cr�ation du nouveau bouton "Ajouter"
        valider = new JButton("Ajouter");
        pBoutons.add(valider);
        valider.addActionListener(this);

        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setTitle("Ajout d'une visite");
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        Object source = evt.getSource();
        // Si on appuye sur entr�e ou sur le bouton, ce code se lance
        if (source == valider || source == JTreference || source == JTdateVisite || source == JTcommentaire || source == JTcodeVisiteur || source == JTcodeMed || source == JTquantite1 || source == JTquantite2) {

            try {
                if (JTquantite1.getText().equals("")) {
                    JTquantite1.setText("0");
                } else if (JTquantite2.getText().equals("")) {
                    JTquantite2.setText("0");
                }
                // Test de chaque entr�e diff�rente et affiche celles manquantes
                if (JTreference.getText().equals("") || JTdateVisite.getText().equals("") || JTcodeVisiteur.getText().equals("") || JTcodeMed.getText().equals("")) {

                    if (JTreference.getText().equals("")) {
                        if (JTdateVisite.getText().equals("")) {
                            if (JTcodeVisiteur.getText().equals("")) {
                                if (JTcodeMed.getText().equals("")) {
                                    throw new Exception("Donn�es obligatoires : reference, date de visite, matricule, medecin");
                                } else {
                                    throw new Exception("Donn�es obligatoires : reference, date de visite, matricule");
                                }
                            } else {
                                if (JTcodeMed.getText().equals("")) {
                                    throw new Exception("Donn�es obligatoires : reference, date de visite, medecin");
                                } else {
                                    throw new Exception("Donn�es obligatoires : reference, date de visite");
                                }
                            }
                        } else {
                            if (JTcodeVisiteur.getText().equals("")) {
                                if (JTcodeMed.getText().equals("")) {
                                    throw new Exception("Donn�es obligatoires : reference, matricule, medecin");
                                } else {
                                    throw new Exception("Donn�es obligatoires : reference, matricule");
                                }
                            } else {
                                if (JTcodeMed.getText().equals("")) {
                                    throw new Exception("Donn�es obligatoires : reference, medecin");
                                } else {
                                    throw new Exception("Donn�e obligatoire : reference");
                                }
                            }
                        }
                    } else {
                        if (JTdateVisite.getText().equals("")) {
                            if (JTcodeVisiteur.getText().equals("")) {
                                if (JTcodeMed.getText().equals("")) {
                                    throw new Exception("Donn�es obligatoires : date de visite, matricule, medecin");
                                } else {
                                    throw new Exception("Donn�es obligatoires : date de visite, matricule");
                                }
                            } else {
                                if (JTcodeMed.getText().equals("")) {
                                    throw new Exception("Donn�es obligatoires : date de visite, medecin");
                                } else {
                                    throw new Exception("Donn�e obligatoire : date de visite");
                                }
                            }
                        } else {
                            if (JTcodeVisiteur.getText().equals("")) {
                                if (JTcodeMed.getText().equals("")) {
                                    throw new Exception("Donn�e obligatoire : matricule, medecin");
                                } else {
                                    throw new Exception("Donn�es obligatoires : matricule");
                                }
                            } else {
                                if (JTcodeMed.getText().equals("")) {
                                    throw new Exception("Donn�es obligatoires : medecin");
                                }
                            }
                        }
                    }

                } else if (!ValidationUtils.isDateValide(JTdateVisite.getText())) {
                    throw new Exception("Le format de la date n'est pas valide");
                } // Test de la longueur de la r�f�rence
                else if (JTreference.getText().length() != 5) {
                    throw new Exception("La r�f�rence ne fait pas 5 caract�res");
                } // Test de la longueur du commentaire
                else if (JTcommentaire.getText().length() > 100) {
                    throw new Exception("Le commentaire fait plus de 100 caract�res");
                } // Test de la longueur du code visiteur
                else if (JTcodeVisiteur.getText().length() > 4) {
                    throw new Exception("Le matricule du visiteur ne fait pas 4 caract�res au maximum");
                } // Test de la longueur du code du m�decin
                else if (JTcodeMed.getText().length() != 4) {
                    throw new Exception("Le code du m�decin ne fait pas 4 caract�res");
                } // Cherche si la r�f�rence de la visite existe d�j�, si oui elle renvoie une erreur
                else if (VisiteDao.rechercher(JTreference.getText()) != null) {
                    throw new Exception("Cette reference de visite existe d�j�");
                } // Cherche si le code du visiteur existe d�j�, si non elle renvoie une erreur
                else if (VisiteurDao.rechercher(JTcodeVisiteur.getText()) == null) {
                    throw new Exception("Ce visiteur n'existe pas");
                } // Cherche si le code de m�decin existe d�j�, si non elle renvoie une erreur
                else if (MedecinDao.rechercher(JTcodeMed.getText()) == null) {
                    throw new Exception("Ce code medecin n'existe pas");
                } // Test si la valeur rentr�e est bien un entier
                else if (!ValidationUtils.estUnEntier(JTquantite1.getText())) {
                    throw new Exception("Une des valeurs de quantit� n'est pas un nombre");
                } else if (!ValidationUtils.estUnEntier(JTquantite2.getText())) {
                    throw new Exception("Une des valeurs de quantit� n'est pas un nombre");
                } // Si aucune erreur n'a �t� cr�e, alors l'ajout de la visite est lanc�
                else {

                    // Recherche du code du m�dicament selon tout les m�dicaments
                    for (Medicament unMedic : lesMedics) {
                        if (unMedic.getNomMedi().equals(comboMedics1.getSelectedItem().toString())) {
                            medicament1 = unMedic.getCodeMedi();
                        }

                        if (unMedic.getNomMedi().equals(comboMedics2.getSelectedItem().toString())) {
                            medicament2 = unMedic.getCodeMedi();
                        }
                    }

                    // Recherche du code du stock selon le visiteur et le code du m�dicament
                    for (Stocker unStock : lesStocks) {
                        if (unStock.getUnVisiteur().getMatricule().equals(JTcodeVisiteur.getText()) && unStock.getUnMedicament().getCodeMedi().equals(medicament1)) {
                            id1 = unStock.getIdStock();
                            if (Integer.parseInt(JTquantite1.getText()) <= unStock.getQteStock()) {
                                quantiteStock1 = unStock.getQteStock() - Integer.parseInt(JTquantite1.getText());
                            } else {
                                throw new Exception("Pour le m�dicament " + comboMedics1.getSelectedItem() + ", la valeur du stock sera inf�rieure � 0");
                            }
                        }
                        if (unStock.getUnVisiteur().getMatricule().equals(JTcodeVisiteur.getText()) && unStock.getUnMedicament().getCodeMedi().equals(medicament2)) {
                            id2 = unStock.getIdStock();
                            if (Integer.parseInt(JTquantite2.getText()) <= unStock.getQteStock()) {
                                quantiteStock2 = unStock.getQteStock() - Integer.parseInt(JTquantite2.getText());
                            } else {
                                throw new Exception("Pour le m�dicament " + comboMedics2.getSelectedItem() + ", la valeur du stock sera inf�rieure � 0");
                            }
                        }
                    }


                    VisiteService.creerVisite(JTreference.getText(), JTdateVisite.getText(), JTcommentaire.getText(), JTcodeVisiteur.getText(), JTcodeMed.getText());

                    if (Integer.parseInt(JTquantite1.getText()) != 0) {
                        StockerService.modifierStock(medicament1, JTcodeVisiteur.getText(), quantiteStock1, id1);
                        OffrirService.ajouterOffre(medicament1, JTreference.getText(), JTquantite1.getText());
                    }

                    if (Integer.parseInt(JTquantite2.getText()) != 0) {
                        StockerService.modifierStock(medicament2, JTcodeVisiteur.getText(), quantiteStock2, id2);
                        OffrirService.ajouterOffre(medicament2, JTreference.getText(), JTquantite2.getText());
                    }

                    JOptionPane.showMessageDialog(this, "La visite a �t� ajout�e");
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, e);
            }
        }
    }
}


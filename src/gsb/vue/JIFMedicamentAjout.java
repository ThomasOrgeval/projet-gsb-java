package gsb.vue;

import gsb.modele.Medicament;
import gsb.modele.Stocker;
import gsb.modele.dao.MedicamentDao;
import gsb.modele.dao.StockerDao;
import gsb.modele.dao.VisiteurDao;
import gsb.service.StockerService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class JIFMedicamentAjout extends JInternalFrame implements ActionListener {

    private static final long serialVersionUID = 1L;
    protected JPanel p, pTexte, pBouton;
    private ArrayList<Medicament> lesMedics = MedicamentDao.retournerCollectionDesMedicament();
    private String medicament, id;
    private ArrayList<Stocker> lesStocks = StockerDao.retournerCollectionDesStocks();
    private boolean newMedic;

    protected JLabel JLmatricule, JLmedic, JLquantite;
    protected JTextField JTquantite, JTmatricule;
    protected JComboBox comboMedics;
    protected JButton valider;

    public JIFMedicamentAjout() {

        p = new JPanel();
        pTexte = new JPanel(new GridLayout(3, 2));
        pBouton = new JPanel();

        JLmatricule = new JLabel("Matricule");
        JLmedic = new JLabel("D�p�t l�gal");
        JLquantite = new JLabel("Quantit�");

        JTmatricule = new JTextField(20);
        JTquantite = new JTextField();


        String[] arrayLesMedics = new String[lesMedics.size()];
        int i = 0;
        for (Medicament unMedic : lesMedics) {
            arrayLesMedics[i] = unMedic.getNomMedi();
            i++;
        }
        comboMedics = new JComboBox(arrayLesMedics);

        pTexte.add(JLmatricule);
        pTexte.add(JTmatricule);
        pTexte.add(JLmedic);
        pTexte.add(comboMedics);
        pTexte.add(JLquantite);
        pTexte.add(JTquantite);

        JTmatricule.addActionListener(this);
        JTquantite.addActionListener(this);

        valider = new JButton("Ajouter");
        pBouton.add(valider);

        p.add(pTexte);
        p.add(pTexte);
        p.add(pBouton);
        Container contentPane = getContentPane();
        contentPane.add(p);

        valider.addActionListener(this);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setTitle("Ajout de m�dicament");
    }

    @Override
    public void actionPerformed(ActionEvent evt) {

        Object source = evt.getSource();
        if (source == JTquantite || source == JTmatricule || source == valider) {
            try {
                if (!comboMedics.getSelectedItem().equals("") && JTquantite.getText().equals("")) {
                    throw new Exception("Le m�dicament 1 a �t� s�lectionn� mais n'a pas de quantit�");
                } else if (JTmatricule.getText().length() > 4) {
                    throw new Exception("Le matricule du visiteur ne fait pas 4 caract�res au maximum");
                } else if (VisiteurDao.rechercher(JTmatricule.getText()) == null) {
                    throw new Exception("Ce visiteur n'existe pas");
                } else if (comboMedics.getSelectedItem().equals("") && !JTquantite.getText().equals("")) {
                    throw new Exception("Le m�dicament 1 a une quantit� mais n'a pas de m�dicament");
                } else {
                    for (Stocker unStock : lesStocks) {
                        newMedic = false;
                        if (JTmatricule.getText().equals(unStock.getUnVisiteur().getMatricule()) && comboMedics.getSelectedItem().equals(unStock.getUnMedicament().getNomMedi())) {
                            newMedic = true;
                            id = unStock.getIdStock();
                        }
                    }
                    if (newMedic) {
                        if (Integer.parseInt(JTquantite.getText()) != 0 || JTquantite.getText().equals("")) {
                            for (Medicament unMedic : lesMedics) {
                                if (unMedic.getNomMedi().equals(comboMedics.getSelectedItem().toString())) {
                                    medicament = unMedic.getCodeMedi();
                                }
                            }
                            StockerService.modifierStock(id, medicament, JTmatricule.getText(), JTquantite.getText());
                        }
                    } else {
                        if (Integer.parseInt(JTquantite.getText()) != 0 || JTquantite.getText().equals("")) {
                            for (Medicament unMedic : lesMedics) {
                                if (unMedic.getNomMedi().equals(comboMedics.getSelectedItem().toString())) {
                                    medicament = unMedic.getCodeMedi();
                                }
                            }
                            StockerService.ajouterStock(medicament, JTmatricule.getText(), JTquantite.getText());
                        }
                    }
                    JOptionPane.showMessageDialog(this, "Les m�dicaments ont �t� ajout�s dans le stock du visiteur");

                }

            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, e);
            }


        }


    }
}

package gsb.vue;

import gsb.modele.Medicament;
import gsb.modele.dao.MedicamentDao;

import javax.swing.*;
import java.awt.*;

public class JIFMedicamentRecap extends JInternalFrame {

    private static final long serialVersionUID = 1L;
    private JTextField JTdepotLegal, JTnomCommercial, JTcomposition, JTeffets, JTcontreIndications, JTcodeFamille, JTlibelleFamille;

    public JIFMedicamentRecap (String codeMedic) {

        JPanel p = new JPanel();
        JPanel pTexte = new JPanel(new GridLayout(7, 2));

        JLabel JLdepotLegal = new JLabel("D�p�t l�gal");
        JLabel JLnomCommercial = new JLabel("Nom commercial");
        JLabel JLcomposition = new JLabel("Composition");
        JLabel JLeffets = new JLabel("Effets");
        JLabel JLcontreIndications = new JLabel("Contre indications");
        JLabel JLcodeFamille = new JLabel("Code famille");
        JLabel JLlibelleFamille = new JLabel("Libell� famille");

        JTdepotLegal = new JTextField(20);
        JTnomCommercial = new JTextField(20);
        JTcomposition = new JTextField(20);
        JTeffets = new JTextField(20);
        JTcontreIndications = new JTextField(20);
        JTcodeFamille = new JTextField(20);
        JTlibelleFamille = new JTextField(20);

        pTexte.add(JLdepotLegal);
        pTexte.add(JTdepotLegal);
        pTexte.add(JLnomCommercial);
        pTexte.add(JTnomCommercial);
        pTexte.add(JLcomposition);
        pTexte.add(JTcomposition);
        pTexte.add(JLeffets);
        pTexte.add(JTeffets);
        pTexte.add(JLcontreIndications);
        pTexte.add(JTcontreIndications);
        pTexte.add(JLcodeFamille);
        pTexte.add(JTcodeFamille);
        pTexte.add(JLlibelleFamille);
        pTexte.add(JTlibelleFamille);

        Medicament leMedicament = MedicamentDao.rechercher(codeMedic);
        remplirText(leMedicament);

        p.add(pTexte);
        Container contentPane = getContentPane();
        contentPane.add(p);

        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setTitle("Fiche m�dicament");

    }

    private void remplirText(Medicament leMedicament) {

        JTdepotLegal.setText(leMedicament.getCodeMedi());
        JTnomCommercial.setText(leMedicament.getNomMedi());
        JTcomposition.setText(leMedicament.getCompositionMedi());
        JTeffets.setText(leMedicament.getEffetsMedi());
        JTcontreIndications.setText(leMedicament.getContreIndicationMedi());
        JTcodeFamille.setText(leMedicament.getFamilleMedi());
        JTlibelleFamille.setText("Cr�er la classe Famille et rechercher en fonction du Dao");

        JTdepotLegal.setEditable(false);
        JTnomCommercial.setEditable(false);
        JTcomposition.setEditable(false);
        JTeffets.setEditable(false);
        JTcontreIndications.setEditable(false);
        JTcodeFamille.setEditable(false);
        JTlibelleFamille.setEditable(false);

    }

}

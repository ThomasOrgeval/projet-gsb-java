package gsb.vue;

import gsb.modele.Medecin;
import gsb.modele.dao.MedecinDao;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

public class JIFMedecinListeDic extends JInternalFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private HashMap<String,Medecin> diccoMedecin;


	protected JPanel p;
	protected JScrollPane scrollPane;
	protected JPanel pSaisie;
	protected JTextField JTcodeMedecin;
	protected JButton JBafficherFiche;
	protected MenuPrincipal fenetreContainer;
	protected JTable table;

	public JIFMedecinListeDic(MenuPrincipal uneFenetreContainer) {

		fenetreContainer = uneFenetreContainer;

		diccoMedecin = MedecinDao.retournerDictionnaireDesMedecins();
		int nbLignes= diccoMedecin.size();
		
		p = new JPanel(); // panneau principal de la fen�tre

		int i=0;
		String[][] data = new String[nbLignes][4] ;
		//for(Medecin unMedecin : lesMedecins){
		
		for (Map.Entry<String,Medecin> uneEntree : diccoMedecin.entrySet()){
			data[i][0] = uneEntree.getValue().getCodeMed();
			data[i][1] = uneEntree.getValue().getNom();
			data[i][2] = uneEntree.getValue().getPrenom();
			data[i][3] = uneEntree.getValue().getLaLocalite().getVille() ;
			i++;
		}
		String[] columnNames = {"Code", "Nom","Prenom","Ville"};
		table = new JTable(data, columnNames);
		table.getSelectionModel().addListSelectionListener(table);
		
		scrollPane = new JScrollPane(table);
		scrollPane.setPreferredSize(new Dimension(400, 200));
		p.add(scrollPane);
		
		pSaisie = new JPanel();
		JTcodeMedecin = new JTextField(20);
		JTcodeMedecin.setMaximumSize(JTcodeMedecin.getPreferredSize());
		JBafficherFiche = new JButton("Afficher Fiche m�decin");
		JBafficherFiche.addActionListener(this); // source d'�venement
		pSaisie.add(JTcodeMedecin);
		pSaisie.add(JBafficherFiche);
		p.add(pSaisie);
		
		// mise en forme de la fen�tre
		Container contentPane = getContentPane();
		contentPane.add(p);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object source = arg0.getSource();
   		if (source == JBafficherFiche){
   			if (diccoMedecin.containsKey(JTcodeMedecin.getText())){
   	   			Medecin unMedecin = diccoMedecin.get(JTcodeMedecin.getText());
   	   			fenetreContainer.ouvrirFenetre(new JIFMedecinFiche(unMedecin));
   			}
   		}
   		if(source == table){
   			JTcodeMedecin.setText((String)table.getValueAt(table.getSelectedRow(), table.getSelectedColumn()));
   			
   		}
	}
}

package gsb.vue;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

class JIFVisiteFiche extends JInternalFrame {

    private static final long serialVersionUID = 1L;
    protected JPanel p, pTexte, pBoutons;

    protected JTable tTableau;
    protected JScrollPane scrollPane;
    protected JTextField JTcodeVisiteur, JTdateVisite;

    JIFVisiteFiche() {
        // Cr�ation des panneaux
        p = new JPanel();  // panneau principal de la fen�tre
        pBoutons = new JPanel();    // panneau supportant les boutons
        pTexte = new JPanel(new GridLayout(3, 2)); // panneau supportant les labels et les textes de 3 lignes et 2 colonnes

        // Cr�ation des labels
        JLabel JLcodeVisiteur = new JLabel("Code visiteur");
        JLabel JLdateVisite = new JLabel("Date visite");

        // Cr�ation des zones de texte
        JTcodeVisiteur = new JTextField(20);
        JTdateVisite = new JTextField();

        // Ajout de la touche entr�e pour effectuer une action plus rapidement
        JTcodeVisiteur.addActionListener((ActionListener) this);
        JTdateVisite.addActionListener((ActionListener) this);

        // Ajout des labels et des zones de texte sur un panneau
        pTexte.add(JLcodeVisiteur);
        pTexte.add(JTcodeVisiteur);
        pTexte.add(JLdateVisite);
        pTexte.add(JTdateVisite);

        // mise en forme de la fen�tre
        p.add(pTexte);
        p.add(pBoutons);
        Container contentPane = getContentPane();
        contentPane.add(p);
    }

}

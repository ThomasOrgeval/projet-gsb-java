package gsb.vue;

import gsb.utils.CountVisite;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.*;

public class JIFVisite extends JInternalFrame {

    private static final long serialVersionUID = 1L;
    protected JPanel p, pTexte, pBoutons, pComboMedic;

    protected JLabel JLreference, JLdateVisite, JLcommentaire, JLcodeVisiteur, JLcodeMed;
    protected JTextField JTreference, JTdateVisite, JTcommentaire, JTcodeVisiteur, JTcodeMed;

    public JIFVisite() {
        // Cr�ation des panneaux
        p = new JPanel();  // panneau principal de la fen�tre
        pBoutons = new JPanel();    // panneau supportant les boutons
        pTexte = new JPanel(new GridLayout(7, 2)); // panneau supportant les labels et les textes de 7 lignes et 2 colonnes
        pComboMedic = new JPanel(new GridLayout(2, 3)); // panneau supportant l'ajout de m�dicament � la visite

        // Cr�ation des labels
        JLreference = new JLabel("R�f�rence");
        JLdateVisite = new JLabel("Date visite");
        JLcommentaire = new JLabel("Commentaire");
        JLcodeVisiteur = new JLabel("Matricule");
        JLcodeMed = new JLabel("Code m�decin");

        // Cr�ation des zones de texte
        JTreference = new JTextField(20);
        JTdateVisite = new JTextField();
        JTcommentaire = new JTextField();
        JTcodeVisiteur = new JTextField();
        JTcodeMed = new JTextField();

        // Initialisation de la r�f�rence de la visite, puis devient non clickable
        JTreference.setText(CountVisite.CountVisite());
        JTreference.setEditable(false);

        // Ajout de la touche entr�e pour effectuer une action plus rapidement
        JTreference.addActionListener((ActionListener) this);
        JTdateVisite.addActionListener((ActionListener) this);
        JTcommentaire.addActionListener((ActionListener) this);
        JTcodeVisiteur.addActionListener((ActionListener) this);
        JTcodeMed.addActionListener((ActionListener) this);

        // Ajout des labels et des zones de texte sur un panneau
        pTexte.add(JLreference);
        pTexte.add(JTreference);
        pTexte.add(JLdateVisite);
        pTexte.add(JTdateVisite);
        pTexte.add(JLcommentaire);
        pTexte.add(JTcommentaire);
        pTexte.add(JLcodeVisiteur);
        pTexte.add(JTcodeVisiteur);
        pTexte.add(JLcodeMed);
        pTexte.add(JTcodeMed);

        // mise en forme de la fen�tre
        p.add(pTexte);
        p.add(pComboMedic);
        p.add(pBoutons);
        Container contentPane = getContentPane();
        contentPane.add(p);

    }

}

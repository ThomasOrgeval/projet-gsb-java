package gsb.service;

import gsb.modele.Medicament;
import gsb.modele.Stocker;
import gsb.modele.Visiteur;
import gsb.modele.dao.MedicamentDao;
import gsb.modele.dao.OffrirDao;
import gsb.modele.dao.StockerDao;
import gsb.modele.dao.VisiteurDao;

public class StockerService {

    public static Stocker rechercherStock (String unID) {

        Stocker unStock = null;
        try {
            if (unID == null) {
                throw new Exception("Donn�e obligatoire : code");
            }
            unStock = StockerDao.rechercher(unID);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return unStock;
    }

    public static int modifierStock (String codeMedic, String matricule, int quantite, String id) {
        int resultat = 0;

        Medicament unMedicament;
        Visiteur unVisiteur;
        Stocker unStock;

        try {
            if (id == null || matricule == null || codeMedic == null) {
                throw new Exception("Donn�es obligatoires : id, reference, code m�decin, quantit�");
            }
            if (id.length() != 5) {
                throw new Exception("L'id ne fait pas 5 caract�res");
            }
            if (VisiteurDao.rechercher(matricule) == null) {
                throw new Exception("Cette reference de visite n'existe pas");
            }
            if (MedicamentDao.rechercher(codeMedic) == null) {
                throw new Exception("Ce code de m�dicament n'existe pas");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            unMedicament = MedicamentDao.rechercher(codeMedic);
            unVisiteur = VisiteurDao.rechercher(matricule);
            unStock = new Stocker(id, unVisiteur, unMedicament, quantite);
            resultat = StockerDao.modifierStock(unStock);
        }
        return resultat;
    }

    public static int ajouterStock (String codeMedic, String matricule, String quantite) {
        int resultat = 0;

        Medicament unMedicament;
        Visiteur unVisiteur;
        Stocker unStock;

        int uneQuantite = Integer.parseInt(quantite);
        String id = "";
        int idInt = StockerDao.retournerCollectionDesStocks().size() + 1;
        if (idInt > 9) {
            id = "s00" + idInt;
        } else if (idInt > 99) {
            id = "s0" + idInt;
        } else if (idInt > 999) {
            id = "s" + idInt;
        } else {
            id = "s000" + idInt;
        }

        try {
            if (id == null || matricule == null || codeMedic == null || quantite == null) {
                throw new Exception("Donn�es obligatoires : id, reference, code m�decin, quantit�");
            }
            if (id.length() != 5) {
                throw new Exception("L'id ne fait pas 5 caract�res");
            }
            if (VisiteurDao.rechercher(matricule) == null) {
                throw new Exception("Ce visiteur n'existe pas");
            }
            if (MedicamentDao.rechercher(codeMedic) == null) {
                throw new Exception("Ce code de m�dicament n'existe pas");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            unMedicament = MedicamentDao.rechercher(codeMedic);
            unVisiteur = VisiteurDao.rechercher(matricule);
            unStock = new Stocker(id, unVisiteur, unMedicament, uneQuantite);
            resultat = StockerDao.ajouterStock(unStock);
        }
        return resultat;
    }

    public static int modifierStock (String id, String codeMedic, String matricule, String quantite) {
        int resultat = 0;

        Medicament unMedicament;
        Visiteur unVisiteur;
        Stocker unStock;

        int uneQuantite = Integer.parseInt(quantite);
        uneQuantite+=StockerDao.rechercher(id).getQteStock();
        try {
            if (id == null || matricule == null || codeMedic == null || quantite == null) {
                throw new Exception("Donn�es obligatoires : id, reference, code m�decin, quantit�");
            }
            if (id.length() != 5) {
                throw new Exception("L'id ne fait pas 5 caract�res");
            }
            if (VisiteurDao.rechercher(matricule) == null) {
                throw new Exception("Ce visiteur n'existe pas");
            }
            if (MedicamentDao.rechercher(codeMedic) == null) {
                throw new Exception("Ce code de m�dicament n'existe pas");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            unMedicament = MedicamentDao.rechercher(codeMedic);
            unVisiteur = VisiteurDao.rechercher(matricule);
            unStock = new Stocker(id, unVisiteur, unMedicament, uneQuantite);
            resultat = StockerDao.modifierStock(unStock);
        }
        return resultat;
    }

}

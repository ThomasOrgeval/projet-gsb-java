package gsb.service;

import gsb.modele.Medicament;
import gsb.modele.dao.MedicamentDao;

public class MedicamentService {

    public static Medicament rechercherMedicament(String unCodeMedi){
        Medicament unMedicament = null;
        try {
            if (unCodeMedi == null){
                throw new Exception("Donn�e obligatoire : code");
            }
            unMedicament = MedicamentDao.rechercher(unCodeMedi);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
        return unMedicament;
    }



}

package gsb.service;

import gsb.modele.Medicament;
import gsb.modele.Offrir;
import gsb.modele.Visite;
import gsb.modele.dao.MedicamentDao;
import gsb.modele.dao.OffrirDao;
import gsb.modele.dao.VisiteDao;

public class OffrirService {

    public static Offrir rechercherOffre(String uneID) {
        Offrir uneOffre = null;
        try {
            if (uneID == null) {
                throw new Exception("Donn�e obligatoire : code");
            }
            uneOffre = OffrirDao.rechercher(uneID);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return uneOffre;
    }

    public static int ajouterOffre(String codeMedic, String reference, String quantite) {
        int resultat = 0;

        Medicament unMedicament;
        Visite uneVisite;
        Offrir uneOffre;

        int uneQuantite = Integer.parseInt(quantite);
        String id = "";
        int idInt = OffrirDao.retournerCollectionDesOffresMedics().size() + 1;
        if (idInt > 9) {
            id = "o00" + idInt;
        } else if (idInt > 99) {
            id = "o0" + idInt;
        } else if (idInt > 999) {
            id = "o" + idInt;
        } else {
            id = "o000" + idInt;
        }

        try {
            if (id == null || reference == null || codeMedic == null || quantite == null) {
                throw new Exception("Donn�es obligatoires : id, reference, code m�decin, quantit�");
            }
            if (id.length() != 5) {
                throw new Exception("L'id ne fait pas 5 caract�res");
            }
            if (VisiteDao.rechercher(reference) == null) {
                throw new Exception("Cette reference de visite n'existe pas");
            }
            if (MedicamentDao.rechercher(codeMedic) == null) {
                throw new Exception("Ce code de m�dicament n'existe pas");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            unMedicament = MedicamentDao.rechercher(codeMedic);
            uneVisite = VisiteDao.rechercher(reference);
            uneOffre = new Offrir(id, uneVisite, unMedicament, uneQuantite);
            resultat = OffrirDao.creer(uneOffre);
        }

        return resultat;
    }

}

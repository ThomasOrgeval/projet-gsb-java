package gsb.service;

import gsb.modele.Medecin;
import gsb.modele.Visite;
import gsb.modele.Visiteur;
import gsb.modele.dao.MedecinDao;
import gsb.modele.dao.VisiteDao;
import gsb.modele.dao.VisiteurDao;
import gsb.utils.ValidationUtils;

public class VisiteService {

    public static Visite rechercherVisite(String uneReference) {
        Visite uneVisite = null;
        try {
            if (uneReference == null) {
                throw new Exception("Donn�e obligatoire : code");
            }
            uneVisite = VisiteDao.rechercher(uneReference);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return uneVisite;
    }

    public static int creerVisite(String reference, String dateVisite, String commentaire, String matricule, String codeMedecin) {
        Visite uneVisite;
        Medecin unMedecin;
        Visiteur unVisiteur;
        int resultat = 0;

        try {
            // Si une donn�e est manquante on l�ve une exception
            if (reference == null || dateVisite == null || matricule == null || codeMedecin == null) {
                throw new Exception("Donn�es obligatoires : reference, date de visite, commentaire, matricule, medecin");
            }
            if (!ValidationUtils.isDateValide(dateVisite)) {
                // Si la date ne poss�de pas le bon format alors on l�ve une exception
                throw new Exception("Le format de la date n'est pas valide");
            }
            if (reference.length() != 5) {
                throw new Exception("La r�f�rence ne fait pas 5 caract�res");
            }
            if (commentaire.length() > 100){
                throw new Exception("Le commentaire fait plus de 100 caract�res");
            }
            if  (matricule.length() > 4){
                throw new Exception("Le matricule du visiteur ne fait pas 4 caract�res au maximum");
            }
            if (codeMedecin.length() != 4){
                throw new Exception("Le code du m�decin ne fait pas 4 caract�res");
            }

            if(VisiteDao.rechercher(reference) != null) {
                throw new Exception("Cette reference de visite existe d�j�");
            }
            if(VisiteurDao.rechercher(matricule) == null) {
                throw new Exception("Ce visiteur n'existe pas");
            }
            if(MedecinDao.rechercher(codeMedecin) == null) {
                throw new Exception("Ce code medecin n'existe pas");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            unMedecin = MedecinDao.rechercher(codeMedecin);
            unVisiteur = VisiteurDao.rechercher(matricule);
            uneVisite = new Visite(reference, dateVisite, commentaire, unVisiteur, unMedecin);
            resultat = VisiteDao.creer(uneVisite);
        }

        return resultat;
    }

}

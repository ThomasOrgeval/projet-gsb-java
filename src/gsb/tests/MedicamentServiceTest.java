package gsb.tests;

import gsb.modele.Medicament;
import gsb.modele.dao.MedicamentDao;
import gsb.service.MedicamentService;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MedicamentServiceTest {

    private static String codeMedic = "PARMOL16";
    private static Medicament unMedic = MedicamentService.rechercherMedicament(codeMedic);

    @Test
    public final void testMedicament () {
        boolean test = false;
        if (MedicamentDao.rechercher(unMedic.getCodeMedi()) != null) {
            test = true;
        }
        assertTrue("Ce médicament existe", test);
    }

    @Test
    public final void testNotNull() {
        boolean test = false;
        if (unMedic.getCodeMedi() != null || unMedic.getNomMedi() != null || unMedic.getFamilleMedi() != null || unMedic.getCompositionMedi() != null || unMedic.getContreIndicationMedi() != null || unMedic.getEffetsMedi() != null || unMedic.getLaVisite().getMatricule() != null) {
            test = true;
        }
        assertTrue(test);
    }

    @Test
    public final void testCodeFamille () {
        int testLongueur = 3;
        if (unMedic.getFamilleMedi().length() <= 3) {
            testLongueur = unMedic.getFamilleMedi().length();
        }
        assertEquals("Au maximum 3 caractères", testLongueur, unMedic.getFamilleMedi().length());
    }

}

package gsb.tests;

import gsb.modele.Offrir;
import gsb.modele.dao.MedicamentDao;
import gsb.modele.dao.VisiteDao;
import gsb.service.OffrirService;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class OffrirServiceTest {

    private static String id = "o0001";
    private static Offrir uneOffreMedic = OffrirService.rechercherOffre(id);

    @Test
    public final void testAjoutOffre () {
        assertEquals("0 offre ajout�e", 0, 0);
        assertEquals("1 offre ajout�e", 1, 1);
    }

    @Test
    public final void testNotNull() {
        boolean bool = false;
        if (uneOffreMedic != null || uneOffreMedic.getOffre() != null || uneOffreMedic.getLaVisite() != null || uneOffreMedic.getLeMedicament() != null || uneOffreMedic.getQteOfferte() != 0) {
            bool = true;
        }
        assertTrue(bool);
    }

    @Test
    public final void testLongueurID () {
        assertEquals("Exactement 5 caract�res", 5, uneOffreMedic.getOffre().length());
    }

    @Test
    public final void testLongueurReference () {
        assertEquals("Exactement 5 caract�res", 5, uneOffreMedic.getLaVisite().getReference().length());
    }

    @Test
    public final void testVisite () {
        boolean test = false;
        if (VisiteDao.rechercher(uneOffreMedic.getLaVisite().getReference()) != null) {
            test = true;
        }
        assertTrue("Cette visite existe", test);
    }

    @Test
    public final void testMedicament () {
        boolean test = false;
        if (MedicamentDao.rechercher(uneOffreMedic.getLeMedicament().getCodeMedi()) != null) {
            test = true;
        }
        assertTrue("Ce m�dicament existe", test);
    }

}

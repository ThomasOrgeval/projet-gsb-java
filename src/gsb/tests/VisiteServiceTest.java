package gsb.tests;

import gsb.modele.Visite;
import gsb.modele.dao.MedecinDao;
import gsb.modele.dao.VisiteurDao;
import gsb.service.VisiteService;

import static org.junit.Assert.*;

import gsb.utils.SQLDateChange;
import org.junit.Test;

public class VisiteServiceTest {

    private static String visite = "v0002";
    private static Visite uneVisite = VisiteService.rechercherVisite(visite);
    private static Visite uneVisite2 = VisiteService.rechercherVisite("v0003");
    private static String date1 = "12/08/2015";

    @Test
    public final void main() {
        System.out.println(uneVisite.getDateVisite());
        System.out.println(uneVisite.getCommentaire());
        System.out.println(uneVisite.getLeVisiteur().getMatricule());
        System.out.println(uneVisite.getLeMedecin().getNom());
        System.out.println(uneVisite.getLeMedecin().getPrenom());
    }

    @Test
    public final void testVisiteAjout() {
        assertEquals("0 visite ajout�e", 0, 0);
        int laVisite = VisiteService.creerVisite("v0002", "22/02/2019", uneVisite.getCommentaire(), uneVisite.getLeVisiteur().getMatricule(), uneVisite.getLeMedecin().getCodeMed());
        assertEquals("1 visite ajout�e", 1, 1);
    }

    @Test
    public final void testNotNull() {
        boolean bool = false;
        if (visite != null || uneVisite.getDateVisite() != null || uneVisite.getCommentaire() != null || uneVisite.getLeVisiteur().getMatricule() != null || uneVisite.getLeMedecin().getCodeMed() != null) {
            bool = true;
        }
        assertTrue(bool);
    }

    @Test
    public final void testFormatDate() {
        assertEquals(date1, "12/08/2015");
        assertEquals(uneVisite.getDateVisite(), "2000-10-01");
    }

    @Test
    public final void testConversionDate() {
        String conversionDate1 = SQLDateChange.SQLDateChange(date1);
        assertEquals(conversionDate1, "2015-08-12");
    }

    @Test
    public final void testLongueurReference() {
        assertEquals("Exactement 5 caract�res", 5, uneVisite.getReference().length());
    }

    @Test
    public final void testMatriculeVisite() {
        if (uneVisite.getLeVisiteur().getMatricule().length() == 2) {
            assertEquals("Au maximum 4 caract�res", 2, uneVisite.getLeVisiteur().getMatricule().length());
            assertEquals("Au maximum 4 caract�res", 2, uneVisite2.getLeVisiteur().getMatricule().length());
        } else if (uneVisite.getLeVisiteur().getMatricule().length() == 3) {
            assertEquals("Au maximum 4 caract�res", 3, uneVisite.getLeVisiteur().getMatricule().length());
            assertEquals("Au maximum 4 caract�res", 3, uneVisite2.getLeVisiteur().getMatricule().length());
        } else if (uneVisite.getLeVisiteur().getMatricule().length() == 4) {
            assertEquals("Au maximum 4 caract�res", 4, uneVisite.getLeVisiteur().getMatricule().length());
            assertEquals("Au maximum 4 caract�res", 4, uneVisite2.getLeVisiteur().getMatricule().length());
        }
    }

    @Test
    public final void testLongueurCommentaire() {
        boolean bool = false;
        if (uneVisite.getCommentaire().length() < 100) {
            bool = true;
        }
        assertTrue(bool);
    }

    @Test
    public final void testLongueurMedecinCode() {
        assertEquals("Longueur de 4 requis", 4, uneVisite.getLeMedecin().getCodeMed().length());
        assertEquals("Longueur de 4 requis", 4, uneVisite2.getLeMedecin().getCodeMed().length());
    }

    @Test
    public final void testVisiteur() {
        boolean test = false;
        if (VisiteurDao.rechercher(uneVisite.getLeVisiteur().getMatricule()) != null) {
            test = true;
        }
        assertTrue("Ce visiteur existe", test);
    }

    @Test
    public final void testMedecin() {
        boolean test = false;
        if (MedecinDao.rechercher(uneVisite.getLeMedecin().getCodeMed()) != null) {
            test = true;
        }
        assertTrue("Ce m�decin existe", test);
    }

}
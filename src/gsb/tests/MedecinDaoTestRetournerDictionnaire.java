package gsb.tests;

import gsb.modele.Medecin;
import gsb.modele.dao.MedecinDao;

import java.util.HashMap;

public class MedecinDaoTestRetournerDictionnaire {

	public static void main(String[] args) {
		
		HashMap<String,Medecin> unDicco = new HashMap<String,Medecin>();
		unDicco =	MedecinDao.retournerDictionnaireDesMedecins();
		if (unDicco.containsKey("m002")){
			System.out.println(unDicco.get("m002").getNom());
		}

	}

}

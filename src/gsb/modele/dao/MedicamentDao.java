package gsb.modele.dao;

import gsb.modele.Localite;
import gsb.modele.Medicament;
import gsb.modele.Visiteur;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;


public class MedicamentDao {

    public static Medicament rechercher(String codeMedi) {
        Medicament unMedicament = null;
        Visiteur unVisiteur;
        ResultSet reqSelection = ConnexionMySql.execReqSelection("select * from MEDICAMENT where DEPOTLEGAL ='" + codeMedi + "'");
        try {
            if (reqSelection.next()) {
                unVisiteur = VisiteurDao.rechercher(reqSelection.getString(8));
                unMedicament = new Medicament(reqSelection.getString(1), reqSelection.getString(2), reqSelection.getString(3), reqSelection.getString(4), reqSelection.getString(5), reqSelection.getDouble(6), reqSelection.getString(7), unVisiteur);
            }
        } catch (Exception e) {
            System.out.println("erreur reqSelection.next() pour la requ�te - select * from MEDICAMENT where DEPOTLEGAL ='" + codeMedi + "'");
            e.printStackTrace();
        }
        ConnexionMySql.fermerConnexionBd();
        return unMedicament;
    }


    public static ArrayList<Medicament> retournerCollectionDesMedicament() {
        ArrayList<Medicament> collectionDesMedicament = new ArrayList<Medicament>();
        ResultSet reqSelection = ConnexionMySql.execReqSelection("select DEPOTLEGAL from MEDICAMENT");
        try {
            while (reqSelection.next()) {
                String codeMedicament = reqSelection.getString(1);
                collectionDesMedicament.add(MedicamentDao.rechercher(codeMedicament));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("erreur retournerCollectionDesMedicament()");
        }
        return collectionDesMedicament;
    }

    public static HashMap<String, Medicament> retournerDictionnaireDesMedicament() {
        HashMap<String, Medicament> diccoDesMedicament = new HashMap<String, Medicament>();
        ResultSet reqSelection = ConnexionMySql.execReqSelection("select DEPOTLEGAL from MEDICAMENT");
        try {
            while (reqSelection.next()) {
                String codeMedicament = reqSelection.getString(1);
                diccoDesMedicament.put(codeMedicament, MedicamentDao.rechercher(codeMedicament));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("erreur retournerDiccoDesMedecins()");
        }
        return diccoDesMedicament;
    }
}
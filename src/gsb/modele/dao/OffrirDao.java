package gsb.modele.dao;

import gsb.modele.Medicament;
import gsb.modele.Offrir;
import gsb.modele.Visite;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class OffrirDao {

    public static Offrir rechercher(String id) {
        Offrir unQteOfferte = null;
        Medicament unMedicament;
        Visite uneVisite;
        ResultSet reqSelection = ConnexionMySql.execReqSelection("select * from OFFRIR where `ID` ='" + id + "'");
        try {
            if (reqSelection.next()) {
                uneVisite = VisiteDao.rechercher(reqSelection.getString(2));
                unMedicament = MedicamentDao.rechercher(reqSelection.getString(3));
                unQteOfferte = new Offrir(reqSelection.getString(1), uneVisite, unMedicament, reqSelection.getInt(4));
            }
        } catch (Exception e) {
            System.out.println("erreur reqSelection.next() pour la requ�te - select * from OFFRIR where `ID` ='" + id + "'");
            e.printStackTrace();
        }
        ConnexionMySql.fermerConnexionBd();
        return unQteOfferte;
    }

    public static ArrayList<Offrir> retournerCollectionDesOffresMedics() {
        ArrayList<Offrir> collectionDesOffresMedics = new ArrayList<Offrir>();
        ResultSet reqSelection = ConnexionMySql.execReqSelection("select ID from OFFRIR");
        try {
            while (reqSelection.next()) {
                String idOffreMedic = reqSelection.getString(1);
                collectionDesOffresMedics.add(OffrirDao.rechercher(idOffreMedic));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("erreur retournerCollectionDesOffresMedics()");
        }
        return collectionDesOffresMedics;
    }

    public static HashMap<String, Offrir> retournerDictionnaireDesMedicsOfferts(String reference) {
        HashMap<String, Offrir> diccoDesMedicsOfferts = new HashMap<String, Offrir>();
        ResultSet reqSelection = ConnexionMySql.execReqSelection("select `ID` from OFFRIR where `REFERENCES` ='" + reference + "'");
        try {
            while (reqSelection.next()) {
                String id = reqSelection.getString(1);
                diccoDesMedicsOfferts.put(id, OffrirDao.rechercher(id));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("erreur retournerDictionnaireDesMedicsOfferts()");
        }
        return diccoDesMedicsOfferts;
    }

    public static int creer(Offrir uneOffre) {
        String id = uneOffre.getOffre();
        String reference = uneOffre.getLaVisite().getReference();
        String depotLegal = uneOffre.getLeMedicament().getCodeMedi();
        int quantite = uneOffre.getQteOfferte();
        return ConnexionMySql.execReqMaj("insert into OFFRIR values('" + id + "','" + reference + "','" + depotLegal + "'," + quantite + ")");
    }

}

package gsb.modele.dao;

import gsb.modele.Medecin;
import gsb.modele.Visite;
import gsb.modele.Visiteur;
import gsb.utils.SQLDateChange;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class VisiteDao {

    public static Visite rechercher(String reference) {
        Visite uneVisite = null;
        Medecin unMedecin;
        Visiteur unVisiteur;
        ResultSet reqSelection = ConnexionMySql.execReqSelection("select * from VISITE where `REFERENCES`='" + reference + "'");
        try {
            if (reqSelection.next()) {
                unMedecin = MedecinDao.rechercher(reqSelection.getString(5));
                unVisiteur = VisiteurDao.rechercher(reqSelection.getString(4));
                uneVisite = new Visite(reqSelection.getString(1), reqSelection.getString(2), reqSelection.getString(3), unVisiteur, unMedecin);
            }
            ;
        } catch (Exception e) {
            System.out.println("erreur reqSelection.next() pour la requ�te - select * from VISITE where `REFERENCES`='" + reference + "'");
            e.printStackTrace();
        }
        ConnexionMySql.fermerConnexionBd();
        return uneVisite;
    }

    public static ArrayList<Visite> retournerCollectionDesVisites() {
        ArrayList<Visite> collectionDesVisites = new ArrayList<Visite>();
        ResultSet reqSelection = ConnexionMySql.execReqSelection("select `REFERENCES` from VISITE");
        try {
            while (reqSelection.next()) {
                String reference = reqSelection.getString(1);
                collectionDesVisites.add(VisiteDao.rechercher(reference));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("erreur retournerCollectionDesVisites()");
        }
        return collectionDesVisites;
    }

    public static HashMap<String, Visite> retournerDictionnaireDesVisites(String id, String date) {
        HashMap<String, Visite> diccoDesVisites = new HashMap<String, Visite>();
        ResultSet reqSelection = ConnexionMySql.execReqSelection("select `REFERENCES` from VISITE where MATRICULE = '" + id + "' and DATE = '" + date + "'");
        try {
            while (reqSelection.next()) {
                String reference = reqSelection.getString(1);
                diccoDesVisites.put(reference, VisiteDao.rechercher(reference));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("erreur retournerDiccoDesVisites()");
        }
        return diccoDesVisites;
    }

    public static int creer(Visite uneVisite) {
        String reference = uneVisite.getReference();
        String dateVisite = uneVisite.getDateVisite();
        String commentaire = uneVisite.getCommentaire();
        String matricule =  uneVisite.getLeVisiteur().getMatricule();
        String codeMed = uneVisite.getLeMedecin().getCodeMed();
        String date = SQLDateChange.SQLDateChange(dateVisite);
        return ConnexionMySql.execReqMaj("insert into Visite values('" + reference + "','" + date + "','" + commentaire + "','" + matricule + "','" + codeMed + "')");
    }

}

package gsb.modele.dao;

import gsb.modele.Medicament;
import gsb.modele.Stocker;
import gsb.modele.Visiteur;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class StockerDao {

    public static Stocker rechercher(String id) {
        Stocker unStock = null;
        Medicament unMedicament;
        Visiteur unVisiteur;
        ResultSet reqSelection = ConnexionMySql.execReqSelection("select * from STOCKER where `ID` ='" + id + "'");
        try {
            if (reqSelection.next()) {
                unVisiteur = VisiteurDao.rechercher(reqSelection.getString(2));
                unMedicament = MedicamentDao.rechercher(reqSelection.getString(3));
                unStock = new Stocker(reqSelection.getString(1), unVisiteur, unMedicament, reqSelection.getInt(4));
            }
        } catch (Exception e) {
            System.out.println("erreur reqSelection.next() pour la requ�te - select * from STOCKER where `ID` ='" + id + "'");
            e.printStackTrace();
        }
        ConnexionMySql.fermerConnexionBd();
        return unStock;
    }

    public static Stocker rechercherUnStock(String medicament, String matricule) {
        Stocker unStock = null;
        Medicament unMedicament;
        Visiteur unVisiteur;
        ResultSet reqSelection = ConnexionMySql.execReqSelection("select * from STOCKER where `MATRICULE` ='" + matricule + "' and `DEPOTLEGAL` ='" + medicament +"'");
        try {
            if (reqSelection.next()) {
                unVisiteur = VisiteurDao.rechercher(reqSelection.getString(2));
                unMedicament = MedicamentDao.rechercher(reqSelection.getString(3));
                unStock = new Stocker(reqSelection.getString(1), unVisiteur, unMedicament, reqSelection.getInt(4));
            }
        } catch (Exception e) {
            System.out.println("erreur reqSelection.next() pour la requ�te - select * from STOCKER where `MATRICULE` ='" + matricule + "' and `DEPOTLEGAL` ='" + medicament +"'");
            e.printStackTrace();
        }
        ConnexionMySql.fermerConnexionBd();
        return unStock;
    }

    public static ArrayList<Stocker> retournerCollectionDesStocks() {
        ArrayList<Stocker> collectionDesStocks = new ArrayList<Stocker>();
        ResultSet reqSelection = ConnexionMySql.execReqSelection("select ID from STOCKER");
        try {
            while (reqSelection.next()) {
                String idStock = reqSelection.getString(1);
                collectionDesStocks.add(StockerDao.rechercher(idStock));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("erreur retournerCollectionDesStocks()");
        }
        return collectionDesStocks;
    }

    public static HashMap<String, Stocker> retournerDictionnaireDesStocks(String matricule) {
        HashMap<String, Stocker> diccoDesStocks = new HashMap<String, Stocker>();
        ResultSet reqSelection = ConnexionMySql.execReqSelection("select `ID` from STOCKER where `MATRICULE` ='" + matricule + "'");
        try {
            while (reqSelection.next()) {
                String id = reqSelection.getString(1);
                diccoDesStocks.put(id, StockerDao.rechercher(id));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("erreur retournerDictionnaireDesStocks()");
        }
        return diccoDesStocks;
    }

    public static int modifierStock(Stocker unStock) {
        String id = unStock.getIdStock();
        String matricule = unStock.getUnVisiteur().getMatricule();
        String depotLegal = unStock.getUnMedicament().getCodeMedi();
        int quantite = unStock.getQteStock();
        ConnexionMySql.execReqMaj("delete from STOCKER where `id`='" + id + "'");
        return ConnexionMySql.execReqMaj("insert into STOCKER values('" + id + "','" + matricule + "','" + depotLegal + "'," + quantite + ")");
    }

    public static int ajouterStock(Stocker unStock) {
        String id = unStock.getIdStock();
        String matricule = unStock.getUnVisiteur().getMatricule();
        String depotLegal = unStock.getUnMedicament().getCodeMedi();
        int quantite = unStock.getQteStock();
        return ConnexionMySql.execReqMaj("insert into STOCKER values('" + id + "','" + matricule + "','" + depotLegal + "'," + quantite + ")");
    }

}

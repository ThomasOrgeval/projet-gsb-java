package gsb.modele;

public class Medicament {
    private String codeMedi, nomMedi, compositionMedi, effetsMedi, contreIndicationMedi, familleMedi;
    private double prixEchantillon;
    private Visiteur laVisite;

    public Medicament(String codeMedi, String nomMedi, String compositionMedi, String effetsMedi, String contreIndicationMedi, double prixEchantillon, String familleMedi, Visiteur laVisite) {
        this.codeMedi = codeMedi;
        this.nomMedi = nomMedi;
        this.compositionMedi = compositionMedi;
        this.effetsMedi = effetsMedi;
        this.contreIndicationMedi = contreIndicationMedi;
        this.prixEchantillon = prixEchantillon;
        this.familleMedi = familleMedi;
        this.laVisite = laVisite;

    }

    public String getCodeMedi() {
        return codeMedi;
    }

    public void setCodeMedi(String codeMedi) {
        this.codeMedi = codeMedi;
    }

    public String getNomMedi() {
        return nomMedi;
    }

    public void setNomMedi(String nomMedi) {
        this.nomMedi = nomMedi;
    }

    public String getCompositionMedi() {
        return compositionMedi;
    }

    public void setCompositionMedi(String compositionMedi) {
        this.compositionMedi = compositionMedi;
    }

    public String getEffetsMedi() {
        return effetsMedi;
    }

    public void setEffetsMedi(String effetsMedi) {
        this.effetsMedi = effetsMedi;
    }

    public String getContreIndicationMedi() {
        return contreIndicationMedi;
    }

    public void setContreIndicationMedi(String contreIndicationMedi) {
        this.contreIndicationMedi = contreIndicationMedi;
    }

    public String getFamilleMedi() {
        return familleMedi;
    }

    public void setFamilleMedi(String familleMedi) {
        this.familleMedi = familleMedi;
    }

    public double getPrixEchantillon() {
        return prixEchantillon;
    }

    public void setPrixEchantillon(double prixEchantillon) {
        this.prixEchantillon = prixEchantillon;
    }

    public Visiteur getLaVisite() {
        return laVisite;
    }

    public void setLaVisite(Visiteur laVisite) {
        this.laVisite = laVisite;
    }
}
package gsb.modele;

public class Stocker {

    protected String idStock;
    protected Visiteur unVisiteur;
    protected Medicament unMedicament;
    protected int qteStock;

    public Stocker(String idStock, Visiteur unVisiteur, Medicament unMedicament, int qteStock) {
        this.idStock = idStock;
        this.unVisiteur = unVisiteur;
        this.unMedicament = unMedicament;
        this.qteStock = qteStock;
    }

    public String getIdStock() {
        return idStock;
    }

    public void setIdStock(String idStock) {
        this.idStock = idStock;
    }

    public Visiteur getUnVisiteur() {
        return unVisiteur;
    }

    public void setUnVisiteur(Visiteur unVisiteur) {
        this.unVisiteur = unVisiteur;
    }

    public Medicament getUnMedicament() {
        return unMedicament;
    }

    public void setUnMedicament(Medicament unMedicament) {
        this.unMedicament = unMedicament;
    }

    public int getQteStock() {
        return qteStock;
    }

    public void setQteStock(int qteStock) {
        this.qteStock = qteStock;
    }
}

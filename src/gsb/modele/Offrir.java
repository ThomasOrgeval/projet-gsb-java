package gsb.modele;

public class Offrir {

    protected Visite laVisite;
    protected Medicament leMedicament;
    protected int qteOfferte;
    protected String offre;

    public Offrir(String offre, Visite laVisite, Medicament leMedicament, int qteOfferte) {
        this.offre = offre;
        this.laVisite = laVisite;
        this.leMedicament = leMedicament;
        this.qteOfferte = qteOfferte;
    }

    public String getOffre() {
        return offre;
    }

    public void setOffre(String offre) {
        this.offre = offre;
    }

    public Visite getLaVisite() {
        return laVisite;
    }

    public void setLaVisite(Visite laVisite) {
        this.laVisite = laVisite;
    }

    public Medicament getLeMedicament() {
        return leMedicament;
    }

    public void setLeMedicament(Medicament leMedicament) {
        this.leMedicament = leMedicament;
    }

    public int getQteOfferte() {
        return qteOfferte;
    }

    public void setQteOfferte(int qteOfferte) {
        this.qteOfferte = qteOfferte;
    }

}

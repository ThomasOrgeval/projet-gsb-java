package gsb.modele;

public class Visite {

    protected String reference;
    protected String dateVisite;
    protected String commentaire;
    protected Visiteur leVisiteur;
    protected Medecin leMedecin;

    public Visite(String reference, String dateVisite, String commentaire, Visiteur leVisiteur, Medecin leMedecin) {
        this.reference = reference;
        this.dateVisite = dateVisite;
        this.commentaire = commentaire;
        this.leVisiteur = leVisiteur;
        this.leMedecin = leMedecin;
    }

    public String getReference() { return reference; }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getDateVisite() {
        return dateVisite;
    }

    public void setDateVisite(String dateVisite) {
        this.dateVisite = dateVisite;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Visiteur getLeVisiteur() {
        return leVisiteur;
    }

    public void setMatricule(String matricule) {
        this.leVisiteur = leVisiteur;
    }

    public Medecin getLeMedecin() {
        return leMedecin;
    }

    public void setCodeMed(Medecin leMedecin) {
        this.leMedecin = leMedecin;
    }
}

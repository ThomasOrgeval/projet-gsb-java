drop database if exists gsb;
create DATABASE gsb;
use gsb;

CREATE TABLE `VISITE`
(
    `REFERENCES`  char(5) not null,
    `DATE`        date    not null,
    `COMMENTAIRE` varchar(100) default null,
    `MATRICULE`   varchar(4)   default null,
    `CODEMED`     char(4) not null,
    primary key (`REFERENCES`)
)ENGINE = InnoDB;

CREATE TABLE `MEDECIN`
(
    `CODEMED`    char(4)     not null,
    `NOM`        varchar(50) not null,
    `PRENOM`     varchar(50) not null,
    `ADRESSE`    varchar(50) not null,
    `CODEPOSTAL` char(5)     not null,
    `TELEPHONE`  varchar(15) default null,
    `POTENTIEL`  varchar(50) default null,
    `SPECIALITE` varchar(50) not null,
    primary key (`CODEMED`)
)ENGINE = InnoDB;

CREATE TABLE `MEDICAMENT`
(
    `DEPOTLEGAL`       varchar(50)  not null,
    `NOMCOMMERCIAL`    varchar(50)  not null,
    `COMPOSITION`      varchar(255) not null,
    `EFFETS`           varchar(255) not null,
    `CONTREINDICATION` varchar(255) not null,
    `PRIXECHANTILLON`  float      default null,
    `CODEFAMILLE`      varchar(3)   not null,
    `MATRICULE`        varchar(4) default null,
    primary key (`DEPOTLEGAL`)
)ENGINE = InnoDB;

CREATE TABLE `VISITEUR`
(
    `MATRICULE`  varchar(4)  not null,
    `NOM`        varchar(50) not null,
    `PRENOM`     varchar(50) not null,
    `LOGIN`      varchar(15) not null,
    `MDP`        char(5)     not null,
    `ADRESSE`    varchar(50) not null,
    `TELEPHONE`  varchar(15) default null,
    `DATEENTREE` date        not null,
    `PRIME`      int         default null,
    `CODEPOSTAL` char(5)     not null,
    `CODEUNIT`   char(4)     not null,
    `DEPOTLEGAL` varchar(50) default null,
    primary key (`MATRICULE`)
)ENGINE = InnoDB;

CREATE TABLE `UNITE`
(
    `CODEUNIT` char(4)     not null,
    `NOM`      varchar(50) not null,
    primary key (`CODEUNIT`)
)ENGINE = InnoDB;

CREATE TABLE `FAMILLE`
(
    `CODEFAMILLE`    varchar(3)   not null,
    `LIBELLEFAMILLE` varchar(100) not null,
    primary key (`CODEFAMILLE`)
)ENGINE = InnoDB;

CREATE TABLE `LOCALITE`
(
    `CODEPOSTAL` char(5)     not null,
    `VILLE`      varchar(50) not null,
    primary key (`CODEPOSTAL`)
)ENGINE = InnoDB;

CREATE TABLE `OFFRIR`
(
    `ID`         char(5)     not null,
    `REFERENCES` char(5)     not null,
    `DEPOTLEGAL` varchar(50) not null,
    `QTEOFFERTE` int         not null,
    primary key (`ID`, `REFERENCES`, `DEPOTLEGAL`)
)ENGINE = InnoDB;

CREATE TABLE `STOCKER`
(
    `ID`         char(5)     not null,
    `MATRICULE`  varchar(5)     not null,
    `DEPOTLEGAL` varchar(50) not null,
    `QTEOFFERTE` int         not null,
    primary key (`ID`, `MATRICULE`, `DEPOTLEGAL`)
)ENGINE = InnoDB;

alter table VISITE
    add constraint foreign key (`CODEMED`) references MEDECIN (`CODEMED`);
alter table VISITE
    add constraint foreign key (`MATRICULE`) references VISITEUR (`MATRICULE`);

alter table MEDECIN
    add constraint foreign key (`CODEPOSTAL`) references LOCALITE (`CODEPOSTAL`);

alter table VISITEUR
    add constraint foreign key (`CODEUNIT`) references UNITE (`CODEUNIT`);

alter table MEDICAMENT
    add constraint foreign key (`CODEFAMILLE`) references FAMILLE (`CODEFAMILLE`);
alter table MEDICAMENT
    add constraint foreign key (`MATRICULE`) references VISITEUR (`MATRICULE`);

alter table OFFRIR
    add constraint foreign key (`REFERENCES`) references VISITE (`REFERENCES`);
alter table OFFRIR
    add constraint foreign key (`DEPOTLEGAL`) references MEDICAMENT (`DEPOTLEGAL`);

alter table STOCKER
    add constraint foreign key (`MATRICULE`) references VISITEUR (`MATRICULE`);
alter table STOCKER
    add constraint foreign key (`DEPOTLEGAL`) references MEDICAMENT (`DEPOTLEGAL`);

INSERT INTO `LOCALITE` (`CODEPOSTAL`, `VILLE`)
VALUES ('13012', 'Allauh'),
       ('13015', 'Marseille'),
       ('13025', 'Berre'),
       ('23100', 'La souteraine'),
       ('23120', 'GrandBourg'),
       ('23200', 'Guéret'),
       ('44000', 'Nantes'),
       ('45000', 'Orléans'),
       ('46000', 'Cahors'),
       ('46123', 'Gramat'),
       ('46200', 'Lalbenque'),
       ('46250', 'Montcuq'),
       ('46512', 'Bessines'),
       ('75011', 'Paris'),
       ('75017', 'Paris'),
       ('75019', 'paris'),
       ('93100', 'Montreuil'),
       ('93210', 'Rosny'),
       ('93230', 'Romainville'),
       ('94000', 'Créteil');


INSERT INTO `MEDECIN` (`CODEMED`, `NOM`, `PRENOM`, `ADRESSE`, `CODEPOSTAL`, `TELEPHONE`, `POTENTIEL`, `SPECIALITE`)
VALUES ('m001', 'SMITH', 'JEAN', '5 rue de la Poste', '23200', '05-55-12-65-45', '', 'Cardiologue'),
       ('m002', 'DURAND', 'PAUL', '34 boulevard Malherbes', '13015', '04-91-67-45-98', 'lol', 'Généraliste'),
       ('m003', 'JACQUES', 'ERIC', '45 rue des Tilleuls', '13025', '04-42-65-43-89', '', 'Rhumatologue'),
       ('m004', 'LE GUEN', 'HENRI', '6 rue de la Paix', '44000', '02-40-88-77-76', '', 'Généraliste'),
       ('m005', 'PAVERNE', 'PATRICK', '89 avenue Jean Jaures', '13015', '04-91-88-54-89', '', 'Dermatologue'),
       ('m006', 'LEMALIN', 'EMILE', '67 rue de la Mairie', '45000', '02-38-67-98-22', '', 'Neurologue'),
       ('m007', 'DECHAVANNE', 'LAURENT', '7 impasse de l Etang', '93210', '01-48-67-55-44', '', 'Pédiatrie'),
       ('m008', 'KAMELIN', 'JACQUES', '9 rue des Mimosas', '23120', '05-55-98-67-44', '', 'Psychiatrie'),
       ('m009', 'TULAN', 'PIERRE', '4 avenue Mendes France', '13015', '04-91-56-34-99', '', 'Allergologue'),
       ('m010', 'BANIZE', 'MARIE', '27 rue des Fleurs', '75019', '01-44-33-56-21', '', 'Gériatrie'),
       ('m011', 'DEVE', 'ANNIE', '8 rue des Platanes', '46000', '05-65-78-76-77', '', 'Généraliste'),
       ('m012', 'CLEMENCEAU', 'MARC', '6 rue Nationale', '46512', '05-61-56-29-35', '', 'Rhumatologue'),
       ('m013', 'BELLENOS', 'MICHEL', '89 rue des Embruns', '45000', '02-38-99-76-34', '', 'Neurologue'),
       ('m014', 'FUMEL', 'ANNE-MARIE', '8 rue des écoles', '44000', '02-40-78-43-22', '', 'Généraliste'),
       ('m015', 'GARDES', 'JEAN-LOUIS', '3 rue du 4 Septembre', '23100', '05-55-78-23-12', '', 'Cardiologue'),
       ('m016', 'GUYOT', 'BENOIT', '5 rue de la Mairie', '46512', '04-50-67-34-22', '', 'Allergologue'),
       ('m017', 'WILSON', 'YANN', '34 rue Paul Eluard', '93210', '01-48-77-98-34', '', 'Généraliste'),
       ('m018', 'MENJOUE', 'GERARD', '21 rue Jules Rimet', '93210', '01-55-76-43-54', '', 'Pédiatrie'),
       ('m019', 'TRANSCEN', 'JEAN', '12 rue des Pinsons', '44000', '02-40-78-45-66', '', 'Généraliste'),
       ('m020', 'LAGADEC', 'FREDERIQUE', '67 rue des Chênes', '13025', '02-48-54-53-44', '', 'Dermatologue');


INSERT INTO `FAMILLE` (`CODEFAMILLE`, `LIBELLEFAMILLE`)
VALUES ('AA', 'Antalgiques en association'),
       ('AAA', 'Antalgiques antipyrétiques en association'),
       ('AAC', 'Antidépresseur d''action centrale'),
       ('AAH', 'Antivertigineux antihistaminique H1'),
       ('ABA', 'Antibiotique antituberculeux'),
       ('ABC', 'Antibiotique antiacnéique local'),
       ('ABP', 'Antibiotique de la famille des béta-lactamines (pénicilline A)'),
       ('AFC', 'Antibiotique de la famille des cyclines'),
       ('AFM', 'Antibiotique de la famille des macrolides'),
       ('AH', 'Antihistaminique H1 local'),
       ('AIM', 'Antidépresseur imipraminique (tricyclique)'),
       ('AIN', 'Antidépresseur inhibiteur sélectif de la recapture de la sérotonine'),
       ('ALO', 'Antibiotique local (ORL)'),
       ('ANS', 'Antidépresseur IMAO non sélectif'),
       ('AO', 'Antibiotique ophtalmique'),
       ('AP', 'Antipsychotique normothymique'),
       ('AUM', 'Antibiotique urinaire minute'),
       ('CRT', 'Corticoïde, antibiotique et antifongique à  usage local'),
       ('HYP', 'Hypnotique antihistaminique'),
       ('PSA', 'Psychostimulant, antiasthénique');


INSERT INTO `MEDICAMENT` (`DEPOTLEGAL`, `NOMCOMMERCIAL`, `CODEFAMILLE`, `COMPOSITION`, `EFFETS`, `CONTREINDICATION`,
                          `PRIXECHANTILLON`)
VALUES ('3MYC7', 'TRIMYCINE', 'CRT', 'Triamcinolone (acétonide) + Néomycine + Nystatine',
        'Ce médicament est un corticoïde à  activité forte ou très forte associé à  un antibiotique et un antifongique, utilisé en application locale dans certaines atteintes cutanées surinfectées.',
        'Ce médicament est contre-indiqué en cas d''allergie à  l''un des constituants, d''infections de la peau ou de parasitisme non traités, d''acné. Ne pas appliquer sur une plaie, ni sous un pansement occlusif.',
        NULL),
       ('ADIMOL9', 'ADIMOL', 'ABP', 'Amoxicilline + Acide clavulanique',
        'Ce médicament, plus puissant que les pénicillines simples, est utilisé pour traiter des infections bactériennes spécifiques.',
        'Ce médicament est contre-indiqué en cas d''allergie aux pénicillines ou aux céphalosporines.', NULL),
       ('AMOPIL7', 'AMOPIL', 'ABP', 'Amoxicilline',
        'Ce médicament, plus puissant que les pénicillines simples, est utilisé pour traiter des infections bactériennes spécifiques.',
        'Ce médicament est contre-indiqué en cas d''allergie aux pénicillines. Il doit être administré avec prudence en cas d''allergie aux céphalosporines.',
        NULL),
       ('AMOX45', 'AMOXAR', 'ABP', 'Amoxicilline',
        'Ce médicament, plus puissant que les pénicillines simples, est utilisé pour traiter des infections bactériennes spécifiques.',
        'La prise de ce médicament peut rendre positifs les tests de dépistage du dopage.', NULL),
       ('AMOXIG12', 'AMOXI Gé', 'ABP', 'Amoxicilline',
        'Ce médicament, plus puissant que les pénicillines simples, est utilisé pour traiter des infections bactériennes spécifiques.',
        'Ce médicament est contre-indiqué en cas d''allergie aux pénicillines. Il doit être administré avec prudence en cas d''allergie aux céphalosporines.',
        NULL),
       ('APATOUX22', 'APATOUX Vitamine C', 'ALO', 'Tyrothricine + Tétracaïne + Acide ascorbique (Vitamine C)',
        'Ce médicament est utilisé pour traiter les affections de la bouche et de la gorge.',
        'Ce médicament est contre-indiqué en cas d''allergie à  l''un des constituants, en cas de phénylcétonurie et chez l''enfant de moins de 6 ans.',
        NULL),
       ('BACTIG10', 'BACTIGEL', 'ABC', 'Erythromycine',
        'Ce médicament est utilisé en application locale pour traiter l''acné et les infections cutanées bactériennes associées.',
        'Ce médicament est contre-indiqué en cas d''allergie aux antibiotiques de la famille des macrolides ou des lincosanides.',
        NULL),
       ('BACTIV13', 'BACTIVIL', 'AFM', 'Erythromycine',
        'Ce médicament est utilisé pour traiter des infections bactériennes spécifiques.',
        'Ce médicament est contre-indiqué en cas d''allergie aux macrolides (dont le chef de file est l''érythromycine).',
        NULL),
       ('BITALV', 'BIVALIC', 'AAA', 'Dextropropoxyphène + Paracétamol',
        'Ce médicament est utilisé pour traiter les douleurs d''intensité modérée ou intense.',
        'Ce médicament est contre-indiqué en cas d''allergie aux médicaments de cette famille, d''insuffisance hépatique ou d''insuffisance rénale.',
        NULL),
       ('CARTION6', 'CARTION', 'AAA',
        'Acide acétylsalicylique (aspirine) + Acide ascorbique (Vitamine C) + Paracétamol',
        'Ce médicament est utilisé dans le traitement symptomatique de la douleur ou de la fièvre.',
        'Ce médicament est contre-indiqué en cas de troubles de la coagulation (tendances aux hémorragies), d''ulcère gastroduodénal, maladies graves du foie.',
        NULL),
       ('CLAZER6', 'CLAZER', 'AFM', 'Clarithromycine',
        'Ce médicament est utilisé pour traiter des infections bactériennes spécifiques. Il est également utilisé dans le traitement de l''ulcère gastro-duodénal, en association avec d''autres médicaments.',
        'Ce médicament est contre-indiqué en cas d''allergie aux macrolides (dont le chef de file est l''érythromycine).',
        NULL),
       ('DEPRIL9', 'DEPRAMIL', 'AIM', 'Clomipramine',
        'Ce médicament est utilisé pour traiter les épisodes dépressifs sévères, certaines douleurs rebelles, les troubles obsessionnels compulsifs et certaines énurésies chez l''enfant.',
        'Ce médicament est contre-indiqué en cas de glaucome ou d''adénome de la prostate, d''infarctus récent, ou si vous avez reà§u un traitement par IMAO durant les 2 semaines précédentes ou en cas d''allergie aux antidépresseurs imipraminiques.',
        NULL),
       ('DIMIRTAM6', 'DIMIRTAM', 'AAC', 'Mirtazapine',
        'Ce médicament est utilisé pour traiter les épisodes dépressifs sévères.',
        'La prise de ce produit est contre-indiquée en cas de d''allergie à  l''un des constituants.', NULL),
       ('DOLRIL7', 'DOLORIL', 'AAA', 'Acide acétylsalicylique (aspirine) + Acide ascorbique (Vitamine C) + Paracétamol',
        'Ce médicament est utilisé dans le traitement symptomatique de la douleur ou de la fièvre.',
        'Ce médicament est contre-indiqué en cas d''allergie au paracétamol ou aux salicylates.', NULL),
       ('DORNOM8', 'NORMADOR', 'HYP', 'Doxylamine',
        'Ce médicament est utilisé pour traiter l''insomnie chez l''adulte.',
        'Ce médicament est contre-indiqué en cas de glaucome, de certains troubles urinaires (rétention urinaire) et chez l''enfant de moins de 15 ans.',
        NULL),
       ('EQUILARX6', 'EQUILAR', 'AAH', 'Méclozine',
        'Ce médicament est utilisé pour traiter les vertiges et pour prévenir le mal des transports.',
        'Ce médicament ne doit pas être utilisé en cas d''allergie au produit, en cas de glaucome ou de rétention urinaire.',
        NULL),
       ('EVILR7', 'EVEILLOR', 'PSA', 'Adrafinil',
        'Ce médicament est utilisé pour traiter les troubles de la vigilance et certains symptomes neurologiques chez le sujet agé.',
        'Ce médicament est contre-indiqué en cas d''allergie à  l''un des constituants.', NULL),
       ('INSXT5', 'INSECTIL', 'AH', 'Diphénydramine',
        'Ce médicament est utilisé en application locale sur les piqûres d''insecte et l''urticaire.',
        'Ce médicament est contre-indiqué en cas d''allergie aux antihistaminiques.', NULL),
       ('JOVAI8', 'JOVENIL', 'AFM', 'Josamycine',
        'Ce médicament est utilisé pour traiter des infections bactériennes spécifiques.',
        'Ce médicament est contre-indiqué en cas d''allergie aux macrolides (dont le chef de file est l''érythromycine).',
        NULL),
       ('LIDOXY23', 'LIDOXYTRACINE', 'AFC', 'Oxytétracycline +Lidocaïne',
        'Ce médicament est utilisé en injection intramusculaire pour traiter certaines infections spécifiques.',
        'Ce médicament est contre-indiqué en cas d''allergie à  l''un des constituants. Il ne doit pas être associé aux rétinoïdes.',
        NULL),
       ('LITHOR12', 'LITHORINE', 'AP', 'Lithium',
        'Ce médicament est indiqué dans la prévention des psychoses maniaco-dépressives ou pour traiter les états maniaques.',
        'Ce médicament ne doit pas être utilisé si vous êtes allergique au lithium. Avant de prendre ce traitement, signalez à  votre médecin traitant si vous souffrez d''insuffisance rénale, ou si vous avez un régime sans sel.',
        NULL),
       ('PARMOL16', 'PARMOCODEINE', 'AA', 'Codéine + Paracétamol',
        'Ce médicament est utilisé pour le traitement des douleurs lorsque des antalgiques simples ne sont pas assez efficaces.',
        'Ce médicament est contre-indiqué en cas d''allergie à  l''un des constituants, chez l''enfant de moins de 15 Kg, en cas d''insuffisance hépatique ou respiratoire, d''asthme, de phénylcétonurie et chez la femme qui allaite.',
        NULL),
       ('PHYSOI8', 'PHYSICOR', 'PSA', 'Sulbutiamine',
        'Ce médicament est utilisé pour traiter les baisses d''activité physique ou psychique, souvent dans un contexte de dépression.',
        'Ce médicament est contre-indiqué en cas d''allergie à  l''un des constituants.', NULL),
       ('PIRIZ8', 'PIRIZAN', 'ABA', 'Pyrazinamide',
        'Ce médicament est utilisé, en association à  d''autres antibiotiques, pour traiter la tuberculose.',
        'Ce médicament est contre-indiqué en cas d''allergie à  l''un des constituants, d''insuffisance rénale ou hépatique, d''hyperuricémie ou de porphyrie.',
        NULL),
       ('POMDI20', 'POMADINE', 'AO', 'Bacitracine',
        'Ce médicament est utilisé pour traiter les infections oculaires de la surface de l''oeil.',
        'Ce médicament est contre-indiqué en cas d''allergie aux antibiotiques appliqués localement.', NULL),
       ('TROXT21', 'TROXADET', 'AIN', 'Paroxétine',
        'Ce médicament est utilisé pour traiter la dépression et les troubles obsessionnels compulsifs. Il peut également être utilisé en prévention des crises de panique avec ou sans agoraphobie.',
        'Ce médicament est contre-indiqué en cas d''allergie au produit.', NULL),
       ('TXISOL22', 'TOUXISOL Vitamine C', 'ALO', 'Tyrothricine + Acide ascorbique (Vitamine C)',
        'Ce médicament est utilisé pour traiter les affections de la bouche et de la gorge.',
        'Ce médicament est contre-indiqué en cas d''allergie à  l''un des constituants et chez l''enfant de moins de 6 ans.',
        NULL),
       ('URIEG6', 'URIREGUL', 'AUM', 'Fosfomycine trométamol',
        'Ce médicament est utilisé pour traiter les infections urinaires simples chez la femme de moins de 65 ans.',
        'La prise de ce médicament est contre-indiquée en cas d''allergie à  l''un des constituants et d''insuffisance rénale.',
        NULL);

INSERT INTO `UNITE` (`CODEUNIT`, `NOM`)
values ('SW', 'SWISS'),
       ('BO', 'BOURDIN');

INSERT INTO `VISITEUR` (`MATRICULE`, `NOM`, `PRENOM`, `LOGIN`, `MDP`, `ADRESSE`, `CODEPOSTAL`, `DATEENTREE`,
                        `CODEUNIT`)
VALUES ('a131', 'Villechalane', 'Louis', 'lvillachane', 'jux7g', '8 rue des Charmes', '46000', '2005-12-21 00:00:00',
        'SW'),
       ('a17', 'Andre', 'David', 'dandre', 'oppg5', '1 rue Petit', '46200', '1998-11-23 00:00:00', 'BO'),
       ('a55', 'Bedos', 'Christian', 'cbedos', 'gmhxd', '1 rue Peranud', '46250', '1995-01-12 00:00:00', 'BO'),
       ('a93', 'Tusseau', 'Louis', 'ltusseau', 'ktp3s', '22 rue des Ternes', '46123', '2000-05-01 00:00:00', 'SW'),
       ('b13', 'Bentot', 'Pascal', 'pbentot', 'doyw1', '11 allée des Cerises', '46512', '1992-07-09 00:00:00', 'BO'),
       ('b16', 'Bioret', 'Luc', 'lbioret', 'hrjfs', '1 Avenue gambetta', '46000', '1998-05-11 00:00:00', 'SW'),
       ('b19', 'Bunisset', 'Francis', 'fbunisset', '4vbnd', '10 rue des Perles', '93100', '1987-10-21 00:00:00', 'BO'),
       ('b25', 'Bunisset', 'Denise', 'dbunisset', 's1y1r', '23 rue Manin', '75019', '2010-12-05 00:00:00', 'SW'),
       ('b28', 'Cacheux', 'Bernard', 'bcacheux', 'uf7r3', '114 rue Blanche', '75017', '2009-11-12 00:00:00', 'SW'),
       ('b34', 'Cadic', 'Eric', 'ecadic', '6u8dc', '123 avenue de la République', '75011', '2008-09-23 00:00:00', 'BO'),
       ('b4', 'Charoze', 'Catherine', 'ccharoze', 'u817o', '100 rue Petit', '75019', '2005-11-12 00:00:00', 'SW'),
       ('b50', 'Clepkens', 'Christophe', 'cclepkens', 'bw1us', '12 allée des Anges', '93230', '2003-08-11 00:00:00',
        'BO'),
       ('b59', 'Cottin', 'Vincenne', 'vcottin', '2hoh9', '36 rue Des Roches', '93100', '2001-11-18 00:00:00', 'SW'),
       ('c14', 'Daburon', 'François', 'fdaburon', '7oqpv', '13 rue de Chanzy', '94000', '2002-02-11 00:00:00', 'SW'),
       ('c3', 'De', 'Philippe', 'pde', 'gk9kx', '13 rue Barthes', '94000', '2010-12-14 00:00:00', 'BO'),
       ('c54', 'Debelle', 'Michel', 'mdebelle', 'od5rt', '181 avenue Barbusse', '93210', '2006-11-23 00:00:00', 'BO'),
       ('d13', 'Debelle', 'Jeanne', 'jdebelle', 'nvwqq', '134 allée des Joncs', '44000', '2000-05-11 00:00:00', 'SW'),
       ('d51', 'Debroise', 'Michel', 'mdebroise', 'sghkb', '2 Bld Jourdain', '44000', '2001-04-17 00:00:00', 'BO'),
       ('e22', 'Desmarquest', 'Nathalie', 'ndesmarquest', 'f1fob', '14 Place d Arc', '45000', '2005-11-12 00:00:00',
        'SW'),
       ('e24', 'Desnost', 'Pierre', 'pdesnost', '4k2o5', '16 avenue des Cèdres', '23200', '2001-02-05 00:00:00', 'BO'),
       ('e39', 'Dudouit', 'Frédéric', 'fdudouit', '44im8', '18 rue de l église', '23120', '2000-08-01 00:00:00', 'SW'),
       ('e49', 'Duncombe', 'Claude', 'cduncombe', 'qf77j', '19 rue de la tour', '23100', '1987-10-10 00:00:00', 'SW'),
       ('e5', 'Enault-Pascreau', 'Céline', 'cenault', 'y2qdu', '25 place de la gare', '23200', '1995-09-01 00:00:00',
        'BO'),
       ('e52', 'Eynde', 'Valérie', 'veynde', 'i7sn3', '3 Grand Place', '13015', '1999-11-01 00:00:00', 'BO'),
       ('f21', 'Finck', 'Jacques', 'jfinck', 'mpb3t', '10 avenue du Prado', '13012', '2001-11-10 00:00:00', 'SW'),
       ('f39', 'Frémont', 'Fernande', 'ffremont', 'xs5tq', '4 route de la mer', '13012', '1998-10-01 00:00:00', 'BO'),
       ('f4', 'Gest', 'Alain', 'agest', 'dywvt', '30 avenue de la mer', '13025', '1985-11-01 00:00:00', 'SW');

INSERT INTO `VISITE` (`REFERENCES`, `DATE`, `COMMENTAIRE`, `MATRICULE`, `CODEMED`)
VALUES ('v0001', '2002-01-20', '', 'b59', 'm001'),
       ('v0002', '2000-10-01', '', 'e39', 'm007'),
       ('v0003', '2001-05-10 00:00:00', '', 'e49', 'm005'),
       ('v0004', '2000-07-11 00:00:00', '', 'd13', 'm004'),
       ('v0005', '2006-12-22 00:00:00', '', 'c54', 'm008'),
       ('v0006', '2005-12-10 00:00:00', '', 'e22', 'm010'),
       ('v0007', '2006-02-03 00:00:00', '', 'e22', 'm014'),
       ('v0008', '2008-10-27 00:00:00', '', 'b34', 'm020'),
       ('v0009', '2001-06-18 00:00:00', '', 'e49', 'm005'),
       ('v0010', '2007-01-20 00:00:00', '', 'c54', 'm008'),
       ('v0011', '2002-02-13 00:00:00', '', 'f21', 'm011'),
       ('v0012', '2002-10-01 00:00:00', '', 'f39', 'm015'),
       ('v0013', '2002-03-10 00:00:00', '', 'f21', 'm012'),
       ('v0014', '2008-11-03 00:00:00', '', 'b34', 'm020'),
       ('v0015', '2006-04-20 00:00:00', '', 'e22', 'm010'),
       ('v0016', '2003-10-14 00:00:00', '', 'f21', 'm001'),
       ('v0017', '2002-07-04 00:00:00', '', 'e49', 'm005'),
       ('v0018', '2009-12-11 00:00:00', '', 'b28', 'm002'),
       ('v0019', '2001-01-10 00:00:00', '', 'e39', 'm007'),
       ('v0020', '2010-01-05 00:00:00', '', 'b28', 'm011'),
       ('v0021', '2000-09-03 00:00:00', '', 'd13', 'm004'),
       ('v0022', '2010-02-05 00:00:00', '', 'b28', 'm002'),
       ('v0023', '2002-06-02 00:00:00', '', 'b13', 'm009');

insert into `OFFRIR` (`ID`, `REFERENCES`, `DEPOTLEGAL`, `QTEOFFERTE`)
values ('o0001', 'v0001', 'EVILR7', 4),
       ('o0002', 'v0001', 'CARTION6', 6),
       ('o0003', 'v0002', 'TXISOL22', 8);

insert into `STOCKER` (ID, MATRICULE, DEPOTLEGAL, QTEOFFERTE)
VALUES ('s0001', 'a131', 'EVILR7', 8),
       ('S0002', 'a131', 'CARTION6', 6);
